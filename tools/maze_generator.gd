extends Node

enum WALL{
	N = 1
	E = 2
	S = 4
	W = 8
}

#The maze' properties
export var map_size : Vector2
export var map_start : Vector2
#var map_seed : String 
export(float, 0, 1) var acyclicity : float = 0

var walls : = {
	Vector2(0,-1) : WALL.N,
	Vector2(1,0) : WALL.E,
	Vector2(0,1): WALL.S,
	Vector2(-1,0): WALL.W
	}

onready var tile_size : TileMap = $walls.cell_size

signal generated

func _ready() -> void:
	randomize()
	generate_maze(randi())
	erase_walls(acyclicity)
	_make_exit()
	emit_signal("generated")

	print("Maze started at: ", g.maze_start)
	print("Maze ended at: ", g.maze_end)
	#generate_maze()

#func _input(_event: InputEvent) -> void:
	#if Input.is_action_just_pressed("ui_accept"):
		#print(generate_maze())
		#erase_walls(acyclicity)
		#print(generate_maze(map_seed.hash()))

func check_neighbors(cell : Vector2, unvisited_cells : Array) -> Array:
	var list : Array = []
	for neighbor in walls.keys():
		if cell + neighbor in unvisited_cells:
			list.append(cell + neighbor)
	return list

func generate_maze(maze_seed : int = 0) -> int:
	var unvisited : Array = []
	var stack : Array = []
	$walls.clear()
	
	if !maze_seed:
		maze_seed = randi()
	seed(maze_seed)

	#Make all of the cells a solid wall and mark as unvisited
	for x in map_size.x:
		for y in map_size.y:
			unvisited.append(Vector2(x, y))
			$walls.set_cellv(Vector2(x, y), WALL.N|WALL.E|WALL.S|WALL.W)
	
	#Set the starting cell
	var current_cell := map_start
	unvisited.erase(current_cell)
	g.maze_start = current_cell

	#Walls generator
	while unvisited:#!unvisited.empty():
		var neighbors : Array= check_neighbors(current_cell, unvisited)
		
		if neighbors.size() > 0: #!neighbors.empty():
			#Pick one of the neighbors
			var next_cell : Vector2 = neighbors[randi() % neighbors.size()]
			stack.append(current_cell)
			
			#Remove walls
			var dir : Vector2 = next_cell - current_cell #Returns a Vector from NESW
			var current_wall_i : int = $walls.get_cellv(current_cell) - walls[dir]
			var next_wall_i : int = $walls.get_cellv(next_cell) - walls[-dir]

			$walls.set_cellv(current_cell, current_wall_i)
			$walls.set_cellv(next_cell, next_wall_i)
			

			current_cell = next_cell
			unvisited.erase(current_cell)
		elif !stack.empty():



			current_cell = stack.pop_back()
		#print($walls.get_cellv(current_cell))
		#yield(get_tree(), "idle_frame")
	
	#print("Maze generated : ", map_seed)
	return maze_seed

#Set the exit from the 3 furthest cells from start
func set_maze_end(furthest_cells : Array) -> Vector2:
	var exit : int = randi() % furthest_cells.size()
	g.maze_end = furthest_cells[exit] as Vector2
	return furthest_cells[exit]
	

func erase_walls(intensity : float) -> void:
	var index := 0

	for _i in range(int(map_size.x*map_size.y*intensity)):
		var cell : Vector2
		var neighbor : Vector2
		#print("i = ", index)
		#print("intensity: ", int(map_size.x*map_size.y*intensity))
		#Remove one wall from the edge to act as an exit
		if index == 0:
			var exit = randi() % 2 
			
			match exit:
				0:#Remove the furthest wall east
					
					cell = Vector2(map_size.x, int(rand_range(0,map_size.y)))
					neighbor  = walls.keys()[3] as Vector2
					#print("Cell Vector: ", cell0
					
				1:#Remove the furthest wall south
					cell = Vector2(int(rand_range(0,map_size.x)), map_size.y)
					neighbor  = walls.keys()[0] as Vector2
			g.maze_end = cell
			print("Location: ", cell)
		elif index != 0:
			cell = Vector2(int(rand_range(1, map_size.x-1)),
							int(rand_range(1, map_size.y-1)))
			#print("Others: ", cell)
		
			neighbor  = walls.keys()[randi() % walls.size()] as Vector2

		#Check for walls
		if $walls.get_cellv(cell) &  walls[neighbor]:
			var wall = $walls.get_cellv(cell) - walls[neighbor]
			var wall_neighbor = $walls.get_cellv(cell + neighbor) - walls[-neighbor]

			$walls.set_cellv(cell, wall)
			$walls.set_cellv(cell + neighbor, wall_neighbor)
			#yield(get_tree(), "idle_frame")
		index += 1

func _make_exit() -> void:
	print("Generating exit...") 
	var end : Area2D = Area2D.new()
	end.name = "exit"
	end.position = (g.maze_end * 204) + Vector2(102, 102)

	var end_bounds : CollisionShape2D = CollisionShape2D.new()
	end_bounds.shape = RectangleShape2D.new()
	end_bounds.shape.extents = Vector2(102, 102)
	add_child(end)
	$exit.add_child(end_bounds)

	print($exit.get_child(0).shape)
	$exit.connect("body_entered", self, "_body_entered")

func _body_entered(body) -> void:
	if body.name == "player":
		g.finished = true
		print("Finished")
	
