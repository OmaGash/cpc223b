extends Node

var time_start : int
var time_end : int
var str_elapsed : String
var elapsed : int

func _ready():
	time_start = OS.get_unix_time()


func _process(delta: float) -> void:
	if !g.finished:
		time_end = OS.get_unix_time()
		elapsed = time_end - time_start
		var minutes := elapsed / 60
		var seconds := elapsed % 60
		str_elapsed  = "%02d : %02d" % [minutes, seconds]
	else:
		g.time_finished = elapsed
		print(g.time_finished)
		_game_finished()
		set_process(false)

	#print("elapsed : ", str_elapsed)
	$gui/timer.text = str_elapsed

func _game_finished() -> void:
	var data : Dictionary = {
		"times_played" : {},
		"score" : {}
	}

	data.score = {"integerValue" : g.time_finished}

	print("asldn")
	$gui/finished.dialog_text = "Finished maze in: " + str(g.time_finished) + " seconds."
	$gui/finished.popup_centered()

	yield($gui/finished, "confirmed")
	#g.send_data("users?documentId=%s" % g.user_info.localId )
