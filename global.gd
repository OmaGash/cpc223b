extends Node

const API_KEY : String = "AIzaSyCQy5zibGsE9m-EevJnuoV7WrAsFXhrUUY"
const PROJECT_ID : String = "cpc223b"

const FIREBASE_LOGIN : String = "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=%s" % API_KEY
const FIREBASE_REG : String = "https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=%s" % API_KEY
const FIREBASE_AUTH : String = "https://identitytoolkit.googleapis.com/v1/accounts:sendOobCode?key=%s" % API_KEY
const FIREBASE_INFO : String = "https://identitytoolkit.googleapis.com/v1/accounts:lookup?key=%s" % API_KEY

const FIRESTORE_URL : String = "https://firestore.googleapis.com/v1/projects/%s/databases/(default)/documents/" % PROJECT_ID

#var token_current : String = ""
var user_info : Dictionary

#This is to keep the method being called multiple times from the signal
#This should be set to false before every first request, and true before every last request
var last_request : bool

#Maze properties
var maze_start : Vector2
var maze_end : Vector2
var finished : bool = false
var time_finished :int

#This includes the transition animation.
func scene_changer(path_to_string : String):
	get_tree().change_scene(path_to_string)

#Login stuff------------------------------------------------------------------------------
func _get_user_info(result : Array) -> Dictionary:
	var result_body : Dictionary = JSON.parse(result[3].get_string_from_ascii()).result as Dictionary
	return result_body

func _request_headers() -> PoolStringArray:
	return PoolStringArray([
		"Content-Type: application/json",
		"Authorization: Bearer %s" % user_info.idToken
	])

func register(email : String, password : String, http : HTTPRequest) -> void:
	
	var body : Dictionary = {
		"email" : email,
		"password" : password,
		"returnSercureToken" : true
	}
	
	last_request = false
	http.request(FIREBASE_REG, [], true, HTTPClient.METHOD_POST, to_json(body))
	var result : Array = yield(http, "request_completed") as Array
	if result[1] == 200:
		user_info = _get_user_info(result)
	else:
		print("[global.gd] Error: ", result[1])
		return
	
	#Verify the email to log in
	body.clear()
	body = {
		"requestType": "VERIFY_EMAIL",
		"idToken" : user_info.idToken
	}
	
	last_request = true
	http.request(FIREBASE_AUTH, [], true, HTTPClient.METHOD_POST, to_json(body))
	result = yield(http, "request_completed") as Array
	if result[1] == 200:
		print("Authentication sent")
		#oobCode = _get_oobCode(result)
		user_info.clear()
	return true

func login(email : String, password : String, http : HTTPRequest) -> void:
	#Login if email is verified
	var body : Dictionary = {
		"email" : email,
		"password" : password,
		"returnSecureToken" : true
	}
	
	last_request = false
	http.request(FIREBASE_LOGIN, [], true, HTTPClient.METHOD_POST, to_json(body))
	var result : Array = yield(http, "request_completed") as Array
	if result[1] == 200:
		user_info = _get_user_info(result)
		#print("global.gd: Login successful")
		#print("global.gd: ")
		#print(user_info)
	else:#If user doesnt exist
		#print("global.gd: body=" + str(body))
		#print("global.gd: user_info=" + str(user_info))
		#print("global.gd: something went wrong[" + str(result[1]) + "]")
		
		#Emit the signal to the login handler
		last_request = true
		emit_signal("request_commpleted")
		return
	body.clear()
	#Check if email is verified
	body = {
		"idToken" : user_info.idToken
	}
	
	last_request = true
	http.request(FIREBASE_INFO, [], true, HTTPClient.METHOD_POST, to_json(body))
	result = yield(http, "request_completed") as Array
#/Login stuff-----------------------------------------------------------------------------
#Database stuff---------------------------------------------------------------------------

func send_data(path : String, data : Dictionary, http : HTTPRequest, update : bool = true) -> void:
	var document : Dictionary = {
		"fields" : data
	}
	var body : String = to_json(document)
	var url : String = FIRESTORE_URL + path
	
	#Update data if true
	if update: http.request(url, _request_headers(), true, HTTPClient.METHOD_PATCH, body)
	#Overwrite data if not
	else : http.request(url, _request_headers(), true, HTTPClient.METHOD_POST, body)

func get_data(path : String, http : HTTPRequest) -> void:
	var url : String = FIRESTORE_URL + path
	http.request(url, _request_headers(), true, HTTPClient.METHOD_GET)

func delete_data(path : String, http : HTTPRequest) -> void:
	var url = FIRESTORE_URL + path
	http.request(url, _request_headers(), true, HTTPClient.METHOD_DELETE)
#/Database stuff--------------------------------------------------------------------------
