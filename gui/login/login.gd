extends Control

onready var dialog : AcceptDialog = $dialog
var admin : bool = false

func _on_login_pressed() -> void:
	admin = false
	g.login($VBoxContainer/email.text, $VBoxContainer/password.text, $HTTPRequest)

func _on_admin_pressed() -> void:
	admin = true
	g.login($VBoxContainer/email.text, $VBoxContainer/password.text, $HTTPRequest)


func _on_register_pressed() -> void:
	g.scene_changer("res://gui/login/register.tscn")

func _on_menu_pressed() -> void:
	g.scene_changer("res://gui/menu.tscn")


func _on_HTTPRequest_request_completed(result: int, response_code: int, headers: PoolStringArray, body: PoolByteArray) -> void:
	#Don't run if the request isn't the last one
	
	var response_body : JSONParseResult = JSON.parse(body.get_string_from_ascii())
	if response_code != 200:
		$dialog.dialog_text =  response_body.result.error.message.capitalize()
		$dialog.popup_centered()
	if !g.last_request:
		return
	
	if response_code == 0:
		#print("Error")
		dialog.window_title = "Error!"
		dialog.dialog_text = "Unable to connect to server."
		dialog.show_modal(true)
		dialog.set_position((get_viewport_rect().size / 2) - (dialog.rect_size / 2))
		dialog.grab_focus()
		return
	elif response_code != 200:
		$dialog.dialog_text =  response_body.result.error.message.capitalize()
		$dialog.popup_centered()
	else:#If login was successful(email and password is correct)
		if response_body.result.users[0].emailVerified:
			$VBoxContainer/title.text = "success"
			yield(get_tree().create_timer(2.0), "timeout")
			dialog.window_title = "Login Successful!"
			dialog.dialog_text = "<Insert main game scene>"
			dialog.show_modal(true)
			dialog.set_position((get_viewport_rect().size / 2) - (dialog.rect_size / 2))
			dialog.grab_focus()
			
			#Switch to the game scene if account is verified
			if !admin: g.scene_changer("res://game/world.tscn")
			elif admin: g.scene_changer("res://tools/admin.tscn")
		else:
			dialog.window_title = "Please Verify!"
			dialog.dialog_text = "Email not verified."
			dialog.show_modal(true)
			dialog.set_position((get_viewport_rect().size / 2) - (dialog.rect_size / 2))
			dialog.grab_focus()
			#g.scene_changer("res://gui/login")
