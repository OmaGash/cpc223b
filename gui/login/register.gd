extends Control

onready var email : LineEdit = $center/email
onready var password : LineEdit = $center/password
onready var confirm : LineEdit = $center/password_confirm
onready var button : Button = $center/register
onready var notif : Label = $center/title
onready var http : HTTPRequest = $http
onready var dialog : AcceptDialog = $dialog

func _on_register_pressed() -> void:
	$dialog.window_title = ""
	if password.text != confirm.text:
		$dialog.window_title = "Error"
		$dialog.dialog_text = "Passwords do not match"
		$dialog.popup_centered()
		return
	elif email.text.empty() or password.text.empty() or confirm.text.empty():
		$dialog.window_title = "Error"
		$dialog.dialog_text = "Complete missing fields."
		$dialog.popup_centered()
		return

	g.register(email.text, password.text, http)

func _on_login_pressed() -> void:
	g.scene_changer("res://gui/login/login.tscn")

func _on_menu_pressed() -> void:
	g.scene_changer("res://gui/menu.tscn")


func _on_http_request_completed(result: int, response_code: int, headers: PoolStringArray, body: PoolByteArray) -> void:
	var response_body : JSONParseResult = JSON.parse(body.get_string_from_ascii())
	if response_code == 0:
		#print("Error")
		dialog.window_title = "Error!"
		dialog.dialog_text = "Unable to connect to server."
		dialog.show_modal(true)
		dialog.set_position((get_viewport_rect().size / 2) - (dialog.rect_size / 2))
		dialog.grab_focus()
		return
	elif response_code != 200:
		$dialog.window_title = "Error: " + str(response_code)
		$dialog.dialog_text =  response_body.result.error.message.capitalize()
		$dialog.popup_centered()
	else:
		$dialog.window_title = "Account successfully created!"
		$dialog.dialog_text = "An email has been sent to your account, please verify your email address before logging in."
		$dialog.popup_centered()
		yield(get_tree().create_timer(2.0), "timeout")
		g.scene_changer("res://gui/login")
