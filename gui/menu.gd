extends Control



func _on_login_pressed() -> void:
	g.scene_changer("res://gui/login/login.tscn")


func _on_signup_pressed() -> void:
	g.scene_changer("res://gui/login/register.tscn")
